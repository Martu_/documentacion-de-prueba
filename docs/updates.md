# Actualizaciones

Puedes llevar a cabo las actualizaciones del sistema desde el panel de control, en el apartado "Actualizar".

Si le das al botón "actualizar" se llevarán a cabo todas las actualizaciones del sistema que estén disponibles en ese momento. Este proceso llevará unos minutos durante los cuales no podrás usar el panel de control.

Si hubiera una nueva Release de MaadiX se actualizará mediante este proceso.


## Actualizaciones de MaadiX

Cuando hay una nueva versión de MaadiX (una nueva Release) lo vas a ver indicado en el panel de control, en el apartado "Actualizar".

Las actualizaciones de MaadiX pueden implicar mejoras en el panel de control, actualizaciones de aplicaciones, corrección de bugs, implementación de nuevas aplicaciones, etc.

Se recomienda mantener el sistema actualizado con la última release de MaadiX para tener una mejor experiencia y evitar problemas de seguridad.


## Actualizaciones automáticas

Además, MaadiX mantiene un sistema de actualizaciones automáticas que mantienen el sistema actualizado de automática sin tener que ir al panel de control cada poco tiempo para actualizar.

Cuando sale una nueva release de MaadiX si es necesario actualizar de forma manual desde el panel de control, ya que MaadiX no lo hace automáticamente.


## Reinicio del sistema

Cuando se llevan a cabo ciertas actualizaciones es necesario que el servidor se reinicie. Esto ocurre, por ejemplo, cuando se actualiza el Kernel.

En estos casos saldrá un aviso en el panel de control que indicará que es necesario reiniciar el sistema para que se carguen las ultimas actualizaciones.

![](img/updates/AvisoReboot.png)

Para reiniciar el servidor solo hay que darle al botón "Reiniciar".

Aviso: durante el proceso de reinicio todos los servicios se detendrán (incluido el servidor web, el correo electrónico, etc). No debería tardar más de unos pocos minutos.

## Actualizaciones de las aplicaciones en MaadiX

Las aplicaciones que instalas en MaadiX se mantendrán actualizadas siempre que actualices cada vez que salga una nueva Release de MaadiX.

El resto de actualizaciones se harán de forma automática y mantendrán el sistema seguro y actualizado.
