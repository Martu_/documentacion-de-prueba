.. MaadiX-docs documentation master file, created by
   sphinx-quickstart on Mon Aug  3 14:25:37 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Tutoriales y documentación
===================================

.. toctree::
   :maxdepth: 2
   :hidden:

   panel-de-control
   updates
   applications
   users
   dominios
   dns
   email
   mysql
   etherpad
   fail2ban
   jitsi-meet
   lool
   mail-man
   mailtrain
   ncloud
   onlyoffice
   vpn
   create-web
   wordpress
   trash
   coturn
   security
   fqdn


Documentación completa sobre el uso de MaadiX.

[Web oficial de MaadiX](https://maadix.net/)

!!! Warning "Nota"

   Estás consultando la documentación para la Versión 2.0 del Panel de Control.
   Para la Versión 1.0 consulta esta página: [Documentación para la Versión 1.0](https://docs.maadix.net/)

¿Qué es MaadiX?
---------------

MaadiX es una herramienta que permite habilitar con un clic aplicaciones de código abierto en un servidor propio. Además, las puedes administrar a través de una interfaz gráfica sin necesidad de **conocimientos técnicos** o **grandes inversiones**.
En esta guía encontrarás indicaciones para el uso de la interfaz gráfica de administración.

¿Cómo funciona?
---------------
MaadiX integra una serie de configuraciones de sistema estandarizadas, que permiten administrar servidores virtuales propios y aplicaciones desde una interfaz gráfica fácil e intuitiva. Permite instalar aplicaciones avanzadas como Jitsi Meet, OpenVPN, Etherpad, servidor de correo, Mailman, Let's Encrypt u Nexctloud entre otras, todo en un simple clic. Con MaadiX, podrás disponer de una nube propia independiente, privada y segura.

Panel de Control
----------------
El panel de control es la interfaz que permite ejecutar distintas tareas de administración y mantenimiento sin necesidad de ejecutar comandos en la terminal. Podrás instalar y administrar aplicaciones, crear cuentas, dominios, cuentas de correo electrónico, otorgar acceso al sistema a otras personas con diferentes tipos de privilegios, ver estadísticas básicas del sistema entre otras operaciones.

Los datos
---------
Todos los servidores tienen instalado un directorio donde se almacenarán los datos de cuentas, dominios, cuentas de correo electrónico y aplicaciones (técnicamente hablando, se trata de un directorio OpenLDAP).
El panel de control es la interfaz gráfica para administrar este directorio, y el sistema sacará del mismo la información necesaria para su configuración. De esta forma, el panel de control no necesita los privilegios necesarios para escribir directamente esta información en el sistema.

Cambios desde la Versión 1.0
----------------------------
Si has actualizado la versión de MaadiX desde una instalación anterior, notarás algunos cambios no solo en la apariencia de la interfaz gráfica, sino también en alguna funcionalidad:

* A partir de esta versión se ha eliminado la posibilidad de visitar el panel de control desde cualquier dominio creado en el servidor. Solo será disponible a través del dominio principal del servidor (FQDN).
* Se ha incrementado el número mínimo de caracteres requeridos para las contraseñas en 10.
