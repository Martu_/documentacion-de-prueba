# Aplicaciones

## Instalar aplicaciones

Desde el panel de control podrás instalar todas las aplicaciones disponibles en MaadiX. Puedes consultar el listado desde la página '**Instalar aplicaciones**'  

![Instalar aplicaciones](img/es/install-apps.png)

## Desactivar aplicaciones   

Las aplicaciones pueden ser desactivadas o reactivadas desde la página '**Mis Aplicaciones** -> **Ver todas**'.  

Cuando desactivas una aplicación los datos no se borran, así que si la vuelves a activar en otro momento recuperarás la configuración anterior.  

![Ver aplicaciones](img/es/view-applications.png)

## Otras aplicaciones

Si quieres instalar otras aplicaciones no listadas en el panel de control de MaadiX puedes hacerlo por tu propia cuenta ya que tienes pleno acceso al sistema. Sin embargo, MaadiX no se responsabiliza de posibles riesgos de seguridad que puedan implicar.

El servicio de soporte que se presta en cualquiera de los planes de MaadiX no incluye soporte para aplicaciones adicionales.

Si necesitas una asistencia específica para la instalación y mantenimiento de aplicaciones adicionales ponte en contacto con el equipo de MaadiX para valorar un presupuesto a medida.

## Archivos de configuración

De forma general, la configuración de las aplicaciones instaladas a través del panel de control se hará a través de sus propios paneles de administración (a través del navegador) que trae cada herramienta, sin necesidad de tocar archivos de configuración del sistema.

Recuerda que si modificas archivos de configuración de los servicios que MaadiX trae integrados en el sistema (como Apache, fail2ban, mysql, ssh, etc), éstos se van a sobre-escribir con la configuración que MaadiX pone por defecto cada vez que actualices el sistema (botón "Actualizar" del panel de control).

MaadiX funciona con [Puppet](https://es.wikipedia.org/wiki/Puppet_(software)), un software para la administración de sistemas de forma "declarativa". Puppet se ejecuta en cada actualización y resetea todas las configuraciones que no hayan sido declaradas por MaadiX.

Si necesitas alguna configuración específica que no quieres que se sobre-escriba, ponte en contacto con el equipo de MaadiX.
