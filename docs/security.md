# Seguridad

En el panel de control, en la pestaña 'Sistema', encontrarás una sección de 'Seguridad'. Aquí vas a poder configuración diferentes opciones para reforzar la seguridad para tu servidor.

## SHH

En el apartado 'SSH' podrás configurar el puerto por el que quieres hacer las conexión por ssh. Elige del desplegable el puerto que quieras entre el 2001 y el 2010 (por defecto tendrás el puerto 22). Una vez cambiado, las conexiones por SSH tendrán que incluir el parámetro con el nuevo puerto designado, por ejemplo:

`ssh -p 2001 user@minombreenmaadix.maadix.org`

Además, puedes marcar la casilla 'Deshabilitar acceso ssh con contraseña' para que las conexiones SSH solo puedan hacerse con claves SSH y no con contraseña. Después de habilitar esta opción solo las cuentas (con acceso SSH o SFTP) a que les hayas añadido sus claves públicas van a poder hacer las conexiones SSH o SFTP.

Recuerda que puedes añadir las claves públicas de las cuentas tanto al crearlas como al editarlas. A la cuenta Superusuarix también le puedes añadir clave SSH. Desde MaadiX, lo recomendamos especialmente para esta cuenta.

![](img/ssh-sec.png)

## Correo electrónico

En esta sección puedes configurar que versiones de TLS quieres que tu servidor de correo admita. TLS 1.0 ya no se considera seguro y en breve se eliminará como opción. TLS 1.2 es la opción recomendada.

![](img/mail-sec.png)

Si alguien te envía un correo haciendo uso de una versión de TLS que tu servidor no soporta entonces no podrás recibirlo (por ejemplo, servidores de correo antiguos que no tengan habilitada versiones TLS 1.2). Si escribes un correo a un servidor que no soporta la versión TLS que tiene tu servidor, tampoco podrá recibirlo.


## Servidor web

Igualmente, en esta sección podrás establecer que versión TLS quieres que tu servidor web soporte. TLS 1.0 ya no se considera seguro y en breve se eliminará como opción. TLS 1.2 es la opción recomendada.

![](img/web-sec.png)

Hasta que no le des al botón guardar no se realizarán los cambios.
