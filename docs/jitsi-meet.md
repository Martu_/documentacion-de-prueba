# Jitsi Meet

Jitsi Meet es una solución código abierto para videoconferencias que permite invitar a otras perosnas a una conferencia a través de un enlace púbico, además de un Chat integrado, compartir pantalla entre otras funcionalidades.

En la instalación automatizada que proveemos en MaadiX hemos configurado por defecto el uso de una contraseña para poder abrir una nueva sala. Una vez creada, cualquiera puede unirse sin necesidad de tener una cuenta activa, a través de un enlace público (Sigue existiendo la opción de que este enlace esté protegido por contraseña).

Las cuentas autorizadas a abrir nuevas salas se pueden crear de forma muy fácil desde el panel de control.

Hemos implementado esta configuración para permitir a quién instale Jitsi Meet mantener un control sobre la cantidad de salas que se vayan creando y evitar así una sobrecarga en su infraestructura.

Por defecto, sin esta configuración adicional, cualquiera que conozca el enlace de la instalación podría utilizar la herramienta abriendo nuevas salas de conferencia y esto podría llevar fácilmente a una disminución del rendimiento, sobre todo en servidores pequeños, con recursos limitados.

A continuación se detallan los pasos a seguir para la instalación y la asignación de los permisos necesarios para abrir nuevas salas.


## Instalación  

Antes de proceder con la instalación es necesario decidir bajo qué dominio o subdominio se quiere alojar la aplicación. Usaremos como ejemplo el subdominio jitsi.example.com.  

Tendremos que crear una entrada DNS de tipo A, que apunte a la IP del servidor.

 `jitsi.example.com A IP.DE.TU.SERVIDOR`  

Según el proveedor de dominio que tengas, la propagación de los DNS puede tardar entré pocos minutos y unas horas. Una vez los DNS estén propagados podrás proceder a la instalación desde el panel de control.  

Al hacer clic en Jitsi Meet, desde la página 'Instalar aplicaciones', se desplegará un campo en el que tendrás que insertar el nombre del dominio/subdominio que quieres utilizar para instalar la aplicación. En el caso de nuestro ejemplo será jitsi.example.com  

![Install Jitsi Meet](img/es/jitsi/install-jitsi.png)


## Conceder permiso para crear nuevas salas

La cuenta 'Superusuarix' tiene por defecto los permisos necesarios para poder iniciar una nueva sala de conferencia. Sin embargo puedes conceder este permiso a otras cuentas ya existentes o crear nuevas incluso solo para este fin.


1. En la pestaña '**Cuentas ordinarias**'. Puedes editar una cuenta existente o bien crear una nueva.  

2. Una vez Jitsi Meet esté instalada, entre las opciones disponibles verás '*Activar Jitsi Meet*'. Marca la casilla correspondiente para otorgarle el permiso de crear salas de conferencias. Si no tienes instalada la aplicación esta opción no estará disponible. En este ejemplo estamos creando la cuenta 'myuser' a la que asignamos una contraseña.

![Screenshot](img/es/jitsi/enable-jitsi.png)

## Crear sala  

Ahora puedes acceder al dominio de instalación, en nuestro ejemplo jitsi.example.com, que te mostrará una pantalla con un campo para insertar el nombre de una sala, pongamos como ejemplo meeting. Una vez insertado el nombre que quieras asignar haz clic en 'IR' o ENTER

![Screenshot](img/es/jitsi/create-room.png)

Verás una alerta en la que la aplicación pide que confirmes que eres el anfitrión. Haces clic en el botón 'Soy el anfitrión'


![Screenshot](img/es/jitsi/login1-jitsi.png)


En este punto la aplicación te pedirá unas credenciales válidas para poder empezar la videollamada mostrando un formulario de autenticación. Ahí tienes que insertar las credenciales recién creadas, es decir la cuenta myuser y su contraseña. Recuerda que podrías también utilizar la cuenta Superusuarix sin necesidad en este caso de otorgarle expresamente permiso.

![Screenshot](img/es/jitsi/login2-jitsi.png)

Una vez creada la sala podrás compartir de forma pública el enlace e invitar cualquiera sin necesidad de crear más cuentas ni otorgar más permisos.  
