# Crea tu web o aplicación

## Elige tu dominio

Para poder crear un sitio web accesible desde cualquier navegador, necesitarás un dominio o subdominio.

Si ya tienes un dominio propio, MaadiX te permite activarlo fácilmente y configurarlo para usarlo luego en tu página web y aplicaciones. Si todavía no has activado tu dominio en MaadiX, puedes encontrar las instrucciones sobre cómo proceder en el [siguiente tutorial](dominios.md).

Alternativamente, si no dispones de un dominio propio, puedes usar tu *subdominio.maadix.org*, donde 'subdominio' coincide con el nombre que elegiste al adquirir tu servidor MaadiX. Deberás crear tu web o aplicación dentro de la carpeta `/var/www/html/subdominio.maadix.org`, y podrás visitarlo desde el navegador en tu *subdominio.maadix.org*.

En el siguiente tutorial se describen los pasos a seguir en caso de estar en posesión de un dominio propio. No obstante, el proceso es el mismo para usarlo en tu *subdominio.maadix.org* si se tiene en cuenta la información anterior.

## Subir contenidos

Una vez tengas [activado el dominio](dominios.md) para tu nuevo sitio web, tendrás que subir los archivos de tu aplicación a la carpeta que se encuentra en `/var/www/html/midominio.com`.   

Puedes acceder a esta ubicación y subir los archivos fácilmente utilizando un cliente SFTP (Secure File Transfer Protocol). En caso de que no tengas ningún cliente SFTP instalado y no supieras cual escoger, [Filezilla](https://filezilla-project.org/) es uno de los más usados y sencillos.

Como es natural, el cliente SFTP te pedirá una serie de credenciales para conectar con el servidor:

### Con una cuenta SFTP

Si al activar tu dominio le has asignado una cuenta webmaster con solo acceso SFTP ésta no tendrá acceso por terminal. Solo podrá acceder utilizando un clientes SFTP ya que no dispone de acceso por SSH. Esta cuenta será la propietaria de la carpeta `/var/www/html/midominio.com`, de manera que sólo conectando con esta cuenta podrás modificar los archivos que se encuentran en ella.

Las credenciales para la conexión con la cuentas Webmaster son:

* **Servidor**: tu *subdomino.maadix.org*.
* **Protocolo**: SFTP.
* **Modo de acceso**: normal.
* **Nombre de la cuenta**: el nombre de la cuenta Webmaster (Advertencia: sensible a mayúsculas y minúsculas).


![Screenshot](img/sftp-midominio.png)


Cuando se establezca la conexión desde una cuenta SFTP verás una carpeta con su nombre. En ella, encontrarás una carpeta que le da acceso a `/var/www/html/midominio.com`, o varias si es Webmaster de otros dominios aparte de este.

También puede tener ahí otros archivos propios que haya subido o creado anteriormente. Las cuentas Webmaster sólo tienen acceso a esta zona y no a todos los archivos del sistema (como sí tiene la cuenta Superusuarix).

### Con una cuenta SSH

Si como webmaster para tu nuevo dominio has asignado una cuenta con acceso SHH y SFTP, esta sí podrá acceder al servidor tanto por terminal como utilizando un cliente SFTP. La forma de acceder por SFTP es parecida a la descrita en el apartado anterior, pero en este caso la ubicación en la que aterrizas cuando se establece la conexión es diferente. Estas cuentas disponen de un directorio propio en `/home/USERNAME` y al contrario de las cuentas SSH no están enjauladas en él. Pueden acceder a otros directorios y efectuar operaciones de lectura/escritura sobre aquellos archivos de los que sean propietarias.    

Tendrás que navegar a través de las carpetas hasta llegar a `/var/www/html/midominio.com/`, que es donde tendrás que subir o crear los archivos de tu nueva web o aplicación. Otra opción más rápida es escribir la ruta `/var/www/html/midominio.com/` en el campo "Sitio remoto".

Una vez ahí, puedes seguir los mismos pasos descritos para las cuentas SFTP [Webmaster](#domain-folder)

![Screenshot](img/sftp-home.png)


Tendrás que navegar a través de las carpetas hasta llegar a `/var/www/html/midominio.com/`, que es donde tendrás que subir o crear los archivos de tu nueva web o aplicación. Otra opción más rápida es escribir la ruta `/var/www/html/midominio.com/` en el campo "Sitio remoto".

Una vez ahí, puedes seguir los mismos pasos descritos para las cuentas SFTP [Webmaster](#domain-folder)

![Screenshot](img/sftp-home.png)
